//
//  requestCallback.swift
//  network-api
//
//  Created by Edgar Froylan Rodriguez Mondragon on 25/04/18.
//

import Foundation

public protocol RequestCallback {
    func success(response:String)
    func error(response:String)
}
