//
//  Request.swift
//  network-api
//
//  Created by Edgar Froylan Rodriguez Mondragon on 25/04/18.
//

import Alamofire

public class Request {
    
    var responseDelegate:RequestCallback?
    
    public var headers:[String:String]?
    
    public static let singleInstance:Request = {
        let instance = Request()
        return instance
    }()
    
    
    public init() {
        
    }
    
    public func get(url:String,params:[String:Any],responseCallBack:RequestCallback) {
        
        print("Request params -> \((params))")
        print("GET -> \((url))")
        
        Alamofire.request(url, method: .get, parameters: params, headers: self.headers).validate().responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .success:
                print("\((url)) -> response -> \((response.result.description))")
                responseCallBack.success(response: response.result.description)
                break
                
            case .failure:
                print("\((url)) -> response -> Server error")
                responseCallBack.error(response: "Server error")
                break
                
            }
        })
        
    }
    
    public func post(url:String,params:[String:Any],responseCallBack:RequestCallback) {
        
        print("Request params -> \((params))")
        print("POST -> \((url))")
        
        Alamofire.request(url, method: .post, parameters: params, headers: self.headers).validate().responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .success:
                print("\((url)) -> response -> \((response.result.description))")
                responseCallBack.success(response: response.result.description)
                break
                
            case .failure:
                print("\((url)) -> response -> Server error")
                responseCallBack.error(response: "Server error")
                break
                
            }
        })
        
    }
    
}
