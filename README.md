# network-api

[![CI Status](https://img.shields.io/travis/Edgar Froylan Rodriguez Mondragon/network-api.svg?style=flat)](https://travis-ci.org/Edgar Froylan Rodriguez Mondragon/network-api)
[![Version](https://img.shields.io/cocoapods/v/network-api.svg?style=flat)](https://cocoapods.org/pods/network-api)
[![License](https://img.shields.io/cocoapods/l/network-api.svg?style=flat)](https://cocoapods.org/pods/network-api)
[![Platform](https://img.shields.io/cocoapods/p/network-api.svg?style=flat)](https://cocoapods.org/pods/network-api)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

network-api is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'network-api'
```

## Author

Edgar Froylan Rodriguez Mondragon, edddea@gmail.com

## License

network-api is available under the MIT license. See the LICENSE file for more info.
